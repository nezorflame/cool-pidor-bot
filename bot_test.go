package main

import (
	"testing"
	"testing/quick"
)

func Test_getSliceElementByMod(t *testing.T) {
	ss := []any{1, 2, 3, 4, 5}
	f := func(x int) bool {
		if x < 0 {
			x = 0 - x
		}
		se := ss[x%5]
		elem := getSliceElementByMod(ss, x)
		return elem == se
	}
	if err := quick.Check(f, nil); err != nil {
		t.Error(err)
	}
}
