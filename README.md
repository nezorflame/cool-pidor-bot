# cool-pidor-bot [![Go Report Card](https://goreportcard.com/badge/gitlab.com/nezorflame/cool-pidor-bot)](https://goreportcard.com/report/gitlab.com/nezorflame/cool-pidor-bot)

Telegram bot for the `Cool or Pidor` game.

## Usage

Copy the `config.example.toml` and rename it to `cool-pidor-bot.toml`, replacing the required credentials.
You can use a different config name, but make sure to pass it to the binary in `-config` flag.

Possible config locations:

* binary folder
* `/etc`
* `$HOME/.config`

## Installation

Bot requires Go modules as a dependency management tool:

```bash
go install gitlab.com/nezorflame/cool-pidor-bot@latest
```
