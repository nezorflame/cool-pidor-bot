package db

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
	"sync"
	"time"

	bolt "go.etcd.io/bbolt"
)

// Package vars
var (
	ErrNotFound = errors.New("key not found")
	ErrNilValue = errors.New("value is nil")

	bucketName = []byte("global")
)

// DB describes local BoltDB database
type DB struct {
	b       *bolt.DB
	timeout time.Duration
	mtx     sync.RWMutex
}

// NewDB creates new instance of DB
func NewDB(path string, timeout time.Duration) (*DB, error) {
	// open connection to the DB
	log.Println("Creating DB connection")
	opts := bolt.DefaultOptions
	if timeout > 0 {
		opts.Timeout = timeout
	}
	b, err := bolt.Open(path, 0755, opts)
	if err != nil {
		return nil, fmt.Errorf("unable to open DB: %w", err)
	}

	// create global bucket if it doesn't exist yet
	log.Println("Setting the default bucket")
	err = b.Update(func(tx *bolt.Tx) error {
		_, bErr := tx.CreateBucketIfNotExists(bucketName)
		return bErr
	})
	if err != nil {
		return nil, fmt.Errorf("unable to create global bucket: %w", err)
	}

	// return the DB
	db := &DB{b: b, timeout: timeout}
	log.Println("DB initiated")
	return db, nil
}

// Close closes the DB
func (db *DB) Close(delete bool) error {
	db.mtx.Lock()
	defer db.mtx.Unlock()

	log.Println("Closing the DB")
	path := db.b.Path()
	done := make(chan error)
	go func() {
		done <- db.b.Close()
		log.Println("DB closed OK")
		close(done)
	}()
	timer := time.NewTimer(db.timeout)
	if delete {
		defer os.Remove(path)
	}

	select {
	case err := <-done:
		if err != nil {
			return fmt.Errorf("unable to close DB: %w", err)
		}
		return nil
	case <-timer.C:
		return fmt.Errorf("unable to close DB: %w", bolt.ErrTimeout)
	}
}

// Keys returns a list of available keys in the global bucket, sorted alphabetically
func (db *DB) Keys() ([]string, error) {
	db.mtx.RLock()
	defer db.mtx.RUnlock()

	var keys []string
	log.Println("Getting the list of DB current keys")
	err := db.b.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName)
		if b == nil {
			return bolt.ErrBucketNotFound
		}
		return b.ForEach(func(k, v []byte) error {
			if v != nil {
				keys = append(keys, string(k))
			}
			return nil
		})
	})
	if err != nil {
		return nil, fmt.Errorf("unable to get the list of keys from DB: %w", err)
	}
	sort.Strings(keys)
	return keys, nil
}

// Get acquires value from DB by provided key
func (db *DB) Get(key string) ([]byte, error) {
	db.mtx.RLock()
	defer db.mtx.RUnlock()

	var value []byte
	log.Println("Getting value from DB")
	err := db.b.View(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName)
		if b == nil {
			return bolt.ErrBucketNotFound
		}
		k, v := b.Cursor().Seek([]byte(key))
		if k == nil || string(k) != key {
			return ErrNotFound
		} else if v == nil {
			return ErrNilValue
		}
		value = make([]byte, len(v))
		copy(value, v)
		return nil
	})
	if err != nil {
		return nil, fmt.Errorf("unable to get value for key '%s' from DB: %w", key, err)
	}
	log.Println("Got the value")
	return value, nil
}

// Put sets/updates the value in DB by provided bucket and key
func (db *DB) Put(key string, val []byte) error {
	db.mtx.Lock()
	defer db.mtx.Unlock()

	log.Println("Saving the value to DB")
	err := db.b.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName)
		if b == nil {
			return bolt.ErrBucketNotFound
		}
		return b.Put([]byte(key), val)
	})
	if err != nil {
		return fmt.Errorf("unable to put value for key '%s' to DB: %w", key, err)
	}
	return nil
}

// Delete removes the value from DB by provided bucket and key
func (db *DB) Delete(key string) error {
	db.mtx.Lock()
	defer db.mtx.Unlock()

	log.Println("Deleting from DB")
	err := db.b.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket(bucketName)
		if b == nil {
			return bolt.ErrBucketNotFound
		}
		return b.Delete([]byte(key))
	})
	if err != nil {
		return fmt.Errorf("unable to delete value for key '%s' from DB: %w", key, err)
	}
	return nil
}

// Purge removes the bucket from DB
func (db *DB) Purge() error {
	db.mtx.Lock()
	defer db.mtx.Unlock()

	err := db.b.Update(func(tx *bolt.Tx) error {
		return tx.DeleteBucket(bucketName)
	})
	if err != nil {
		return fmt.Errorf("unable to purge global bucket from DB: %w", err)
	}
	return nil
}
