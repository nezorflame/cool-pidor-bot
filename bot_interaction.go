package main

import (
	"errors"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var targets = []string{"cool", "pidor"}

func (b *botClient) join(m *tgbotapi.Message) error {
	game := b.getGame(m.Chat.ID)

	if err := game.Add(NewPlayer(m)); err != nil {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.join_error"), m.MessageID)
		return err
	}

	b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.join"), m.MessageID)
	return b.saveGame(game)
}

func (b *botClient) leave(m *tgbotapi.Message) error {
	game := b.getGame(m.Chat.ID)

	if err := game.Remove(NewPlayer(m)); err != nil {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.leave_error"), m.MessageID)
		return err
	}

	b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.leave"), m.MessageID)
	return b.saveGame(game)
}

func (b *botClient) play(m *tgbotapi.Message) error {
	game := b.getGame(m.Chat.ID)

	// exit if launched on the same day
	if game.PreviousTS > 0 {
		last, now := time.Unix(game.PreviousTS, 0), time.Now()
		if sameDay(last, now, nightStart, nightEnd) {
			b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.game.limit"), m.MessageID)
			return nil
		}
	}

	if err := game.Play(); err != nil {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.game.error"), 0)
		return err
	}

	// print game progress and report
	b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.game.start"), 0)
	b.sendGameMessages(m.Chat.ID)
	b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.game.end"), 0)

	coolWinner, coolNicknameCounter, _ := game.LastCoolPlayer()
	coolNickname := getSliceElementByMod(b.cfg.GetStringSlice("messages.game.cool_nicknames"), coolNicknameCounter)
	pidorWinner, pidorNickNameCounter, _ := game.LastPidorPlayer()
	pidorNickname := getSliceElementByMod(b.cfg.GetStringSlice("messages.game.pidor_nicknames"), pidorNickNameCounter)
	b.sendMessage(m.Chat.ID, fmt.Sprintf(b.cfg.GetString("messages.cool"), coolNickname, coolWinner.FullNameWithMention()), 0)
	b.sendMessage(m.Chat.ID, fmt.Sprintf(b.cfg.GetString("messages.pidor"), pidorNickname, pidorWinner.FullNameWithMention()), 0)

	return b.saveGame(game)
}

func (b *botClient) printWinner(m *tgbotapi.Message, target string) error {
	if target != targets[0] && target != targets[1] {
		return errors.New("target not supported")
	}

	game := b.getGame(m.Chat.ID)
	var (
		lastTargetPlayer *Player
		nicknameCounter  int
		targetLeft       bool
	)
	if target == targets[0] {
		lastTargetPlayer, nicknameCounter, targetLeft = game.LastCoolPlayer()
	} else {
		lastTargetPlayer, nicknameCounter, targetLeft = game.LastPidorPlayer()
	}

	if lastTargetPlayer != nil {
		nickname := getSliceElementByMod(b.cfg.GetStringSlice("messages.game."+target+"_nicknames"), nicknameCounter)
		b.sendMessage(m.Chat.ID, fmt.Sprintf(b.cfg.GetString("messages."+target), nickname, lastTargetPlayer), m.MessageID)
	} else if targetLeft {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages."+target+"_left"), m.MessageID)
	} else {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.no_winner"), m.MessageID)
	}
	return nil
}

func (b *botClient) printMonthWinners() error {
	games, err := b.getAllGames()
	if err != nil {
		return err
	}

	for _, g := range games {
		coolMonthLeaders, pidorMonthLeaders := g.CoolMonthLeaders(), g.PidorMonthLeaders()
		if coolMonthLeaders == "" && pidorMonthLeaders == "" {
			return nil
		}

		b.sendMessage(g.ChatID, fmt.Sprintf(g.cfg.GetString("messages.cool_month"), coolMonthLeaders), 0)
		b.sendMessage(g.ChatID, fmt.Sprintf(g.cfg.GetString("messages.pidor_month"), pidorMonthLeaders), 0)
		g.Clear(false)
		if err = b.saveGame(g); err != nil {
			log.Printf("Unable to save game: %s", err)
		}
	}

	return nil
}

func (b *botClient) printScore(m *tgbotapi.Message, target string) error {
	if target != targets[0] && target != targets[1] {
		return errors.New("target not supported")
	}

	game := b.getGame(m.Chat.ID)
	var scoreboard string
	if target == targets[0] {
		scoreboard = game.CoolScoreboard()
	} else {
		scoreboard = game.PidorScoreboard()
	}

	if scoreboard != "" {
		b.sendMessage(m.Chat.ID, fmt.Sprintf("%s\n\n%s", b.cfg.GetString("messages.score."+target), scoreboard), m.MessageID)
	} else {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.no_scores"), m.MessageID)
	}

	return nil
}

func (b *botClient) printRandom(m *tgbotapi.Message, args []string) error {
	// check args first
	if len(args) != 2 {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.random.error"), m.MessageID)
		return nil
	}
	x, err := strconv.Atoi(args[0])
	if err != nil {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.random.error"), m.MessageID)
		return nil
	}
	y, err := strconv.Atoi(args[1])
	if err != nil {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.random.error"), m.MessageID)
		return nil
	}
	if x >= y {
		b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.random.error"), m.MessageID)
		return nil
	}

	// shuffle numbers and return first one
	var ints []int
	for i := x; i <= y; i++ {
		ints = append(ints, i)
	}
	rand.Shuffle(len(ints), func(i, j int) { ints[i], ints[j] = ints[j], ints[i] })

	b.sendMessage(m.Chat.ID, fmt.Sprintf(b.cfg.GetString("messages.random.answer"), ints[0]), m.MessageID)
	return nil
}
