package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"sort"
	"strings"
	"time"

	"github.com/spf13/viper"
)

const counterLimit = 1000000

// Game represents the CoolPidor game
type Game struct {
	ChatID             int64     `json:"id"`
	Players            []*Player `json:"players"`
	PreviousTS         int64     `json:"previous_ts"`
	LastCoolID         int       `json:"last_cool_id"`
	LastCoolNameCount  int       `json:"last_cool_name_count"`
	LastPidorID        int       `json:"last_pidor_id"`
	LastPidorNameCount int       `json:"last_pidor_name_count"`

	cfg *viper.Viper
}

// NewGame creates new instance of the Game
func NewGame(chatID int64, cfg *viper.Viper) *Game {
	return &Game{
		ChatID:      chatID,
		LastCoolID:  -1,
		LastPidorID: -1,
		cfg:         cfg,
	}
}

// Add adds the Player to the Game
func (g *Game) Add(p *Player) error {
	if p == nil {
		return errors.New("player is nil")
	}

	if _, ok := playerInSlice(p, g.Players); ok {
		return errors.New("player already exists")
	}

	g.Players = append(g.Players, p)
	return nil
}

// Remove removes the Player from the Game
func (g *Game) Remove(p *Player) error {
	if p == nil {
		return errors.New("player is nil")
	}

	i, ok := playerInSlice(p, g.Players)
	if !ok {
		return errors.New("player doesn't exist")
	}

	// clean up if the player was the last cool or pidor guy
	if g.LastCoolID == i {
		g.LastCoolID = -1
	}
	if g.LastPidorID == i {
		g.LastCoolID = -1
	}

	// taken from https://github.com/golang/go/wiki/SliceTricks
	copy(g.Players[i:], g.Players[i+1:])
	g.Players[len(g.Players)-1] = nil
	g.Players = g.Players[:len(g.Players)-1]

	return nil
}

// Clear resets the Game
func (g *Game) Clear(full bool) {
	for i := range g.Players {
		g.Players[i].CoolScore = 0
		g.Players[i].PidorScore = 0
	}

	if full {
		g.PreviousTS = 0
		g.LastCoolID = -1
		g.LastPidorID = -1
	}
}

// Play launches the game
func (g *Game) Play() error {
	playerCount := int64(len(g.Players))
	if playerCount == 0 {
		return errors.New("no players present")
	}
	g.PreviousTS = time.Now().Unix()
	// cool player things
	randInt, _ := rand.Int(rand.Reader, big.NewInt(playerCount))
	g.LastCoolID = int(randInt.Int64())
	randInt, _ = rand.Int(rand.Reader, big.NewInt(counterLimit))
	g.LastCoolNameCount = int(randInt.Int64())
	// pidor player things
	randInt, _ = rand.Int(rand.Reader, big.NewInt(playerCount))
	g.LastPidorID = int(randInt.Int64())
	randInt, _ = rand.Int(rand.Reader, big.NewInt(counterLimit))
	g.LastPidorNameCount = int(randInt.Int64())

	g.Players[g.LastCoolID].CoolScore++
	g.Players[g.LastPidorID].PidorScore++

	return nil
}

// CoolScoreboard creates a formatted string with the scoreboard sorted by players' CoolScore
func (g *Game) CoolScoreboard() string {
	if len(g.Players) == 0 {
		return ""
	}

	coolPlayers := byCoolScore(g.Players)
	sort.Sort(coolPlayers)

	result := fmt.Sprintf("*# | %s | %s*", g.cfg.GetString("messages.score.players"), g.cfg.GetString("messages.score.scores"))
	for i, p := range coolPlayers {
		result += fmt.Sprintf("\n%d. %s (%d)", i+1, p.FullName(), p.CoolScore)
	}

	return result
}

// PidorScoreboard creates a formatted string with the scoreboard sorted by players' PidorScore
func (g *Game) PidorScoreboard() string {
	if len(g.Players) == 0 {
		return ""
	}

	pidorPlayers := byPidorScore(g.Players)
	sort.Sort(pidorPlayers)

	result := fmt.Sprintf("*# | %s | %s*", g.cfg.GetString("messages.score.players"), g.cfg.GetString("messages.score.scores"))
	for i, p := range pidorPlayers {
		result += fmt.Sprintf("\n%d. %s (%d)", i+1, p.FullName(), p.PidorScore)
	}

	return result
}

// CoolMonthLeaders creates a formatted string with the best cool Players of the month
func (g *Game) CoolMonthLeaders() string {
	if len(g.Players) == 0 {
		return ""
	}

	coolPlayers := byCoolScore(g.Players)
	sort.Sort(coolPlayers)
	maxScore := coolPlayers[0].CoolScore
	if maxScore == 0 {
		return ""
	}

	topPlayerNames := make([]string, 0, len(coolPlayers))
	for _, p := range coolPlayers {
		if p.CoolScore == maxScore {
			topPlayerNames = append(topPlayerNames, p.FullNameWithMention())
		}
	}

	return strings.Join(topPlayerNames, ", ")
}

// PidorMonthLeaders creates a formatted string with the best pidor Players of the month
func (g *Game) PidorMonthLeaders() string {
	if len(g.Players) == 0 {
		return ""
	}

	pidorPlayers := byPidorScore(g.Players)
	sort.Sort(pidorPlayers)
	maxScore := pidorPlayers[0].PidorScore
	if maxScore == 0 {
		return ""
	}

	topPlayerNames := make([]string, 0, len(pidorPlayers))
	for _, p := range pidorPlayers {
		if p.PidorScore == maxScore {
			topPlayerNames = append(topPlayerNames, p.FullNameWithMention())
		}
	}

	return strings.Join(topPlayerNames, ", ")
}

// LastCoolPlayer returns last cool Player, its cool nickname's count and if it left the game
func (g *Game) LastCoolPlayer() (*Player, int, bool) {
	if len(g.Players) == 0 {
		return nil, 0, false
	}
	if g.LastCoolID == -1 {
		return nil, 0, true
	}
	return g.Players[g.LastCoolID], g.LastCoolNameCount, false
}

// LastPidorPlayer returns last pidor Player, its pidor nickname's count and if it left the game
func (g *Game) LastPidorPlayer() (*Player, int, bool) {
	if len(g.Players) == 0 {
		return nil, 0, false
	}
	if g.LastPidorID == -1 {
		return nil, 0, true
	}
	return g.Players[g.LastPidorID], g.LastPidorNameCount, false
}
