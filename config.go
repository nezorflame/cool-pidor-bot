package main

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/spf13/viper"
)

const (
	msgEmptyValue = "empty config value '%s'"

	defaultDBTimeout = 2
	defaultTimeout   = 60
	defaultDelay     = 1
	defaultGameDelay = 24

	defaultGameSentenceLimit = 3

	nightStart = 1
	nightEnd   = 9
)

var mandatoryParams = []string{
	"db.path",
	"telegram.token",
	"commands.start",
	"commands.join",
	"commands.leave",
	"commands.play",
	"commands.cool",
	"commands.pidor",
	"commands.score_cool",
	"commands.score_pidor",
	"commands.random",
	"commands.clear",
	"commands.purge",
	"messages.delay",
	"messages.start",
	"messages.join",
	"messages.join_error",
	"messages.leave",
	"messages.leave_error",
	"messages.cool",
	"messages.pidor",
	"messages.cool_month",
	"messages.pidor_month",
	"messages.no_winner",
	"messages.no_scores",
	"messages.cool_left",
	"messages.pidor_left",
	"messages.score.cool",
	"messages.score.pidor",
	"messages.score.players",
	"messages.score.scores",
	"messages.game.start",
	"messages.game.sentences",
	"messages.game.cool_nicknames",
	"messages.game.pidor_nicknames",
	"messages.game.error",
	"messages.game.end",
	"messages.random.answer",
	"messages.random.error",
}

func loadConfig(name string) *viper.Viper {
	if name == "" {
		log.Fatal("empty config name")
	}

	cfg := viper.New()

	cfg.SetConfigName(name)
	cfg.SetConfigType("toml")
	cfg.AddConfigPath("$HOME/.config")
	cfg.AddConfigPath("/etc")
	cfg.AddConfigPath(".")

	if err := cfg.ReadInConfig(); err != nil {
		log.Panicf("Unable to read config file: %v", err)
	}
	cfg.WatchConfig()

	cfg.SetDefault("ctx_timeout", time.Duration(defaultTimeout)*time.Second)
	cfg.SetDefault("night_start", nightStart)
	cfg.SetDefault("night_end", nightEnd)
	cfg.SetDefault("db.timeout", time.Duration(defaultDBTimeout)*time.Second)
	cfg.SetDefault("telegram.timeout", time.Duration(defaultTimeout)*time.Second)
	cfg.SetDefault("messages.delay", time.Duration(defaultDelay)*time.Second)
	cfg.SetDefault("messages.game.delay", time.Duration(defaultGameDelay)*time.Hour)
	cfg.SetDefault("messages.game.sentence_limit", defaultGameSentenceLimit)

	if err := validateConfig(cfg); err != nil {
		log.Panicf("Unable to validate config: %v", err)
	}

	return cfg
}

func validateConfig(cfg *viper.Viper) error {
	if cfg == nil {
		return errors.New("config is nil")
	}

	for _, p := range mandatoryParams {
		if cfg.Get(p) == nil {
			return fmt.Errorf(msgEmptyValue, p)
		}
	}

	if len(cfg.GetStringSlice("messages.game.sentences")) < 1 {
		return errors.New("'messages.game.sentences' should contain at least one sentence")
	}
	if len(cfg.GetStringSlice("messages.game.cool_nicknames")) < 1 {
		return errors.New("'messages.game.cool_nicknames' should contain at least one nickname")
	}
	if len(cfg.GetStringSlice("messages.game.pidor_nicknames")) < 1 {
		return errors.New("'messages.game.pidor_nicknames' should contain at least one nickname")
	}

	return nil
}
