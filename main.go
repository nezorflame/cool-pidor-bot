package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/nezorflame/cool-pidor-bot/db"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"golang.org/x/net/proxy"
)

func main() {
	// load config and do preparation
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	configName := flag.String("config", "config", "Config file name")
	flag.Parse()

	log.Print("Loading config from environment")
	cfg := loadConfig(*configName)

	boltDB, err := db.NewDB(cfg.GetString("db.path"), cfg.GetDuration("db.timeout"))
	if err != nil {
		log.Fatal(err)
	}

	// init graceful stop chan
	log.Print("Initiating system signal watcher")
	gracefulStop := make(chan os.Signal, 1)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		log.Printf("Caught sig %+v, stopping the app", sig)
		cancel()
		if err = boltDB.Close(false); err != nil {
			log.Printf("Unable to close DB: %v", err)
		}
		os.Exit(0)
	}()

	log.Print("Starting the bot")

	// Setup a custom HTTP client with a SOCKS5 proxy dialer (if enabled)
	httpClient := http.DefaultClient
	if proxyAddress := cfg.GetString("proxy.address"); proxyAddress != "" {
		log.Print("Proxy is enabled")
		// enabling proxy
		auth := &proxy.Auth{
			User:     cfg.GetString("proxy.user"),
			Password: cfg.GetString("proxy.pass"),
		}
		dialer, dErr := proxy.SOCKS5("tcp", proxyAddress, auth, proxy.Direct)
		if dErr != nil {
			log.Fatalf("Can't connect to the proxy: %v", dErr)
		}

		httpClient = &http.Client{Transport: &http.Transport{Dial: dialer.Dial}}
		log.Print("Proxy dialer initiated")
	} else {
		log.Print("Proxy is disabled")
	}

	// Init the Telegram bot
	tgClient, err := tgbotapi.NewBotAPIWithClient(cfg.GetString("telegram.token"), tgbotapi.APIEndpoint, httpClient)
	if err != nil {
		log.Fatalf("Unable to init Telegram bot: %v", err)
	}

	bot := &botClient{
		api: tgClient,
		cfg: cfg,
		db:  boltDB,
	}
	log.Print("Bot ready")

	// Listen to the user messages
	bot.listenUpdates(ctx)
}
