package main

import (
	"fmt"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const mentionString = `[@%s](tg://user?id=%d)`

// Player represents the CoolPidor player
type Player struct {
	ID         int64  `json:"id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	UserName   string `json:"user_name"`
	CoolScore  uint   `json:"cool_score"`
	PidorScore uint   `json:"pidor_score"`
}

// NewPlayer creates new instance of the Player
func NewPlayer(m *tgbotapi.Message) *Player {
	username := m.From.UserName
	if username == "" {
		username = m.From.FirstName + " " + m.From.LastName
	}
	return &Player{
		ID:        m.From.ID,
		FirstName: m.From.FirstName,
		LastName:  m.From.LastName,
		UserName:  strings.TrimSpace(username),
	}
}

// FullName reports Player's full name
func (p *Player) FullName() string {
	result := fmt.Sprintf("%s %s", p.FirstName, p.LastName)
	return strings.TrimSpace(result)
}

// FullNameWithMention reports Player's full name with mention
func (p *Player) FullNameWithMention() string {
	mention := fmt.Sprintf(mentionString, p.UserName, p.ID)
	result := fmt.Sprintf("%s %s (%s)", p.FirstName, p.LastName, mention)
	result = strings.Replace(result, "  ", " ", -1)
	return strings.TrimSpace(result)
}

func (p *Player) String() string {
	return p.FullName()
}

type byCoolScore []*Player

func (ps byCoolScore) Len() int           { return len(ps) }
func (ps byCoolScore) Swap(i, j int)      { ps[i], ps[j] = ps[j], ps[i] }
func (ps byCoolScore) Less(i, j int) bool { return ps[i].CoolScore > ps[j].CoolScore }

type byPidorScore []*Player

func (ps byPidorScore) Len() int           { return len(ps) }
func (ps byPidorScore) Swap(i, j int)      { ps[i], ps[j] = ps[j], ps[i] }
func (ps byPidorScore) Less(i, j int) bool { return ps[i].PidorScore > ps[j].PidorScore }

func playerInSlice(p *Player, ps []*Player) (int, bool) {
	if p == nil || len(ps) == 0 {
		return 0, false
	}
	for i := range ps {
		if ps[i].ID == p.ID {
			return i, true
		}
	}
	return 0, false
}
