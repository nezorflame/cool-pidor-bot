package main

import (
	"context"
	"errors"
	"log"
	"math/rand"
	"strings"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/spf13/viper"

	"gitlab.com/nezorflame/cool-pidor-bot/db"
)

type botClient struct {
	cfg     *viper.Viper
	api     *tgbotapi.BotAPI
	db      *db.DB
	playMtx sync.Mutex
}

func (b *botClient) listenUpdates(ctx context.Context) {
	// Setup the updates channel
	conf := tgbotapi.NewUpdate(0)
	conf.Timeout = b.cfg.GetInt("telegram.timeout")
	updates := b.api.GetUpdatesChan(conf)
	defer b.api.StopReceivingUpdates()
	monthTicker := time.NewTicker(time.Minute)
	defer monthTicker.Stop()

	for {
		select {
		case u := <-updates:
			if u.Message == nil {
				continue
			}
			if err := <-b.parseUpdate(u.Message); err != nil {
				log.Println(err)
			}
		case now := <-monthTicker.C:
			if err := b.checkMonth(now); err != nil {
				log.Println(err)
			}
		case <-ctx.Done():
			log.Printf("Exiting the program: %s", ctx.Err())
			return
		}
	}
}

func (b *botClient) parseUpdate(m *tgbotapi.Message) <-chan error {
	errChan := make(chan error)
	go func() {
		defer close(errChan)
		msgParts := strings.Split(m.Text, " ")
		if len(msgParts) == 0 {
			errChan <- errors.New("message is empty")
			return
		}

		cmd := msgParts[0]
		switch {
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.join")):
			errChan <- b.join(m)
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.leave")):
			errChan <- b.leave(m)
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.start")):
			_, err := b.loadGame(m.Chat.ID)
			if err != nil {
				b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.start"), m.MessageID)
				return
			}
			fallthrough
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.play")):
			nightStart, nightEnd := b.cfg.GetInt("night_start"), b.cfg.GetInt("night_end")
			if checkNight(time.Now().Hour(), nightStart, nightEnd) {
				b.sendMessage(m.Chat.ID, b.cfg.GetString("messages.game.night"), m.MessageID)
				return
			}

			b.playMtx.Lock()
			defer b.playMtx.Unlock()

			errChan <- b.play(m)
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.cool")):
			errChan <- b.printWinner(m, "cool")
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.pidor")):
			errChan <- b.printWinner(m, "pidor")
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.score_cool")):
			errChan <- b.printScore(m, "cool")
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.score_pidor")):
			errChan <- b.printScore(m, "pidor")
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.random")):
			errChan <- b.printRandom(m, msgParts[1:])
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.purge")):
			errChan <- b.deleteGame(m.Chat.ID)
		case strings.HasPrefix(cmd, b.cfg.GetString("commands.clear")):
			game := b.getGame(m.Chat.ID)
			game.Clear(true)
			errChan <- b.saveGame(game)
		default:
		}
		log.Println("OK")
	}()
	return errChan
}

func (b *botClient) checkMonth(now time.Time) error {
	todayMonth, tomorrowMonth := now.Month(), now.Add(6*time.Hour).Month()
	if todayMonth != tomorrowMonth {
		return b.printMonthWinners()
	}
	return nil
}

func (b *botClient) sendGameMessages(chatID int64) {
	gameSentences := b.cfg.GetStringSlice("messages.game.sentences")
	limit := b.cfg.GetInt("messages.game.sentence_limit")
	if limit > len(gameSentences) {
		limit = len(gameSentences)
	}

	randomOrder := rand.Perm(len(gameSentences))
	for i := 0; i < limit; i++ {
		msg := gameSentences[randomOrder[i]]
		b.sendMessage(chatID, msg, 0)
	}
}

func (b *botClient) sendMessage(chatID int64, text string, replyID int) {
	time.Sleep(b.cfg.GetDuration("messages.delay"))
	log.Println(text)
	msg := tgbotapi.NewMessage(chatID, text)
	if replyID >= 0 {
		msg.ReplyToMessageID = replyID
	}
	msg.ParseMode = tgbotapi.ModeMarkdown
	_, err := b.api.Send(msg)
	if err != nil {
		log.Printf("Unable to send the message: %v", err)
	}
}

func getSliceElementByMod[T any](ss []T, i int) T {
	index := i % len(ss)
	return ss[index]
}

func checkNight(hour, nightStart, nightEnd int) bool {
	if hour >= nightStart && hour < nightEnd {
		return true
	}
	return false
}

func sameDay(day1, day2 time.Time, nightStart, nightEnd int) bool {
	if day2.Sub(day1) > time.Duration(24)*time.Hour {
		return false
	}

	if day2.YearDay() == day1.YearDay() {
		if day1.Hour() <= nightStart && day2.Hour() >= nightEnd {
			return false
		}
		return true
	}

	if day1.Hour() > day2.Hour() && day1.Hour() >= nightEnd && day2.Hour() < nightStart {
		return true
	}

	return false
}
