package main

import (
	"encoding/json"
	"fmt"
	"strconv"
)

func (b *botClient) getGame(chatID int64) *Game {
	game, err := b.loadGame(chatID)
	if err != nil {
		return NewGame(chatID, b.cfg)
	}
	return game
}

func (b *botClient) getAllGames() ([]*Game, error) {
	ids, err := b.db.Keys()
	if err != nil {
		return nil, fmt.Errorf("unable to get game IDs from DB: %w", err)
	}

	games := make([]*Game, 0, len(ids))
	for _, id := range ids {
		chatID, err := strconv.ParseInt(id, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("unable to parse game ID: %w", err)
		}
		games = append(games, b.getGame(chatID))
	}
	return games, nil
}

func (b *botClient) loadGame(chatID int64) (*Game, error) {
	id := strconv.FormatInt(chatID, 10)

	body, err := b.db.Get(id)
	if err != nil {
		return nil, fmt.Errorf("unable to get JSON from DB: %w", err)
	}

	g := NewGame(chatID, b.cfg)
	if err = json.Unmarshal(body, g); err != nil {
		return nil, fmt.Errorf("unable to unmarshal JSON into game: %w", err)
	}

	return g, nil
}

func (b *botClient) saveGame(g *Game) error {
	if g == nil {
		return nil
	}

	id := strconv.FormatInt(g.ChatID, 10)
	body, err := json.Marshal(g)
	if err != nil {
		return fmt.Errorf("unable to marshal Game into JSON: %w", err)
	}

	if err = b.db.Put(id, body); err != nil {
		return fmt.Errorf("unable to put JSON into DB: %w", err)
	}

	return nil
}

func (b *botClient) deleteGame(chatID int64) error {
	id := strconv.FormatInt(chatID, 10)

	if err := b.db.Delete(id); err != nil {
		return fmt.Errorf("unable to remove record from DB: %w", err)
	}

	return nil
}
